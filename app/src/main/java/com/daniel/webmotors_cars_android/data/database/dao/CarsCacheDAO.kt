package com.daniel.webmotors_cars_android.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.daniel.webmotors_cars_android.data.database.model.CarCache

@Dao
interface CarsCacheDAO {

    @Query("SELECT * FROM car_cache_table")
    suspend fun getCarsCache(): List<CarCache>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun saveCarCache(carCache: CarCache)

    @Query("DELETE FROM car_cache_table")
    suspend fun deleteCarsCache()
}