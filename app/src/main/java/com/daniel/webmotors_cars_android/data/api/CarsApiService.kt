package com.daniel.webmotors_cars_android.data.api

import com.daniel.webmotors_cars_android.data.model.ResponseCars
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CarsApiService {
    @GET("/api/OnlineChallenge/Vehicles")
    suspend fun getCars(@Query("Page") page: Int): Response<ResponseCars>
}