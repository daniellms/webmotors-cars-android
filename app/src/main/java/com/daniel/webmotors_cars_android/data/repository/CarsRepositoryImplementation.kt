package com.daniel.webmotors_cars_android.data.repository

import com.daniel.webmotors_cars_android.data.database.model.CarCache
import com.daniel.webmotors_cars_android.data.datasource.CarsLocalDataSource
import com.daniel.webmotors_cars_android.data.datasource.CarsRemoteDataSource
import com.daniel.webmotors_cars_android.data.model.Car
import com.daniel.webmotors_cars_android.data.model.ResponseCars
import com.daniel.webmotors_cars_android.domain.repository.CarsRepository
import retrofit2.Response

class CarsRepositoryImplementation (private val carsLocalDataSource: CarsLocalDataSource,
                                    private val carsRemoteDataSource: CarsRemoteDataSource): CarsRepository {

    override suspend fun getCars(): ArrayList<Car> {
        return getCarsPageOneApi()
    }

    private suspend fun getCarsPageOneApi(): ArrayList<Car> {
        deleteAllCarsLocal()
        val PAGE_ONE_LIST_OF_CARS = 1
        return getListCarsFromApi(PAGE_ONE_LIST_OF_CARS)
    }

    private suspend fun deleteAllCarsLocal() {
        carsLocalDataSource.deleteCarsCache()
    }

    override suspend fun getListCarsFromLocalDatabase(): ArrayList<Car> {
        val listCarsCache = carsLocalDataSource.getCarsCache()
        return getListCarsConverted(listCarsCache)
    }

    private fun getListCarsConverted(listCarsCache: List<CarCache>): ArrayList<Car> {
        val listCars = ArrayList<Car>()

        for (carCache in listCarsCache) {
            val car = carCache.getCarConverted()
            listCars.add(car)
        }

        return listCars
    }

    private suspend fun getListCarsFromApi(page: Int): ArrayList<Car> {
        val responseListCars = carsRemoteDataSource.getCars(page)
        val listCars = ArrayList<Car>()
        if (responseListCars.code() == 200) {
            for (car in responseListCars.body()!!) {
                listCars.add(car)
                carsLocalDataSource.saveCarCache(car)
            }
        }

        return listCars
    }

    override suspend fun getUpdateListCars(page: Int): Response<ResponseCars> {
        return carsRemoteDataSource.getCars(page)
    }

    override suspend fun saveAllOnLocalDatabase(listCars: ArrayList<Car>) {
        carsLocalDataSource.saveListCarsCache(listCars)
    }

}