package com.daniel.webmotors_cars_android.data.datasource

import androidx.annotation.WorkerThread
import com.daniel.webmotors_cars_android.data.database.dao.CarsCacheDAO
import com.daniel.webmotors_cars_android.data.database.model.CarCache
import com.daniel.webmotors_cars_android.data.model.Car

class CarsLocalDataSource(private val carsCacheDAO: CarsCacheDAO) {

    @WorkerThread
    suspend fun getCarsCache(): List<CarCache> {
        return carsCacheDAO.getCarsCache()
    }

    @WorkerThread
    suspend fun saveCarCache(car: Car) {
        val carCache = car.getCarForCache()
        carsCacheDAO.saveCarCache(carCache)
    }

    @WorkerThread
    suspend fun saveListCarsCache(cars: ArrayList<Car>) {
        for (car in cars) {
            saveCarCache(car)
        }
    }

    @WorkerThread
    suspend fun deleteCarsCache() {
        carsCacheDAO.deleteCarsCache()
    }

}