package com.daniel.webmotors_cars_android.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.daniel.webmotors_cars_android.data.database.dao.CarsCacheDAO
import com.daniel.webmotors_cars_android.data.database.model.CarCache

@Database(entities = arrayOf(CarCache::class), version = 1, exportSchema = false)
abstract class CarsRoomDatabase : RoomDatabase() {
    abstract fun carsCacheDao(): CarsCacheDAO
}