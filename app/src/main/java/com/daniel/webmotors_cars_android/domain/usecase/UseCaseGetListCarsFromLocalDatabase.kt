package com.daniel.webmotors_cars_android.domain.usecase

import com.daniel.webmotors_cars_android.data.model.Car
import com.daniel.webmotors_cars_android.domain.repository.CarsRepository

class UseCaseGetListCarsFromLocalDatabase(private val carsRepository: CarsRepository) {
    suspend fun makeAction(): ArrayList<Car> {
        return carsRepository.getListCarsFromLocalDatabase()
    }
}