package com.daniel.webmotors_cars_android.domain.repository

import com.daniel.webmotors_cars_android.data.model.Car
import com.daniel.webmotors_cars_android.data.model.ResponseCars
import retrofit2.Response

interface CarsRepository {
    suspend fun getCars(): ArrayList<Car>
    suspend fun getListCarsFromLocalDatabase(): ArrayList<Car>
    suspend fun getUpdateListCars(page: Int): Response<ResponseCars>
    suspend fun saveAllOnLocalDatabase(listCars: ArrayList<Car>)
}