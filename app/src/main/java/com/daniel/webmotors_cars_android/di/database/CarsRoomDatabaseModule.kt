package com.daniel.webmotors_cars_android.di.database

import android.content.Context
import androidx.room.Room
import com.daniel.webmotors_cars_android.data.database.CarsRoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class CarsRoomDatabaseModule {

    @Singleton
    @Provides
    fun provideCarsRoomDatabase(@ApplicationContext context: Context): CarsRoomDatabase {
        return Room.databaseBuilder(context.applicationContext,
            CarsRoomDatabase::class.java,
            "cars_database").allowMainThreadQueries().build()
    }
}