package com.daniel.webmotors_cars_android.di.usecase

import com.daniel.webmotors_cars_android.domain.repository.CarsRepository
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetCars
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
class UseCaseGetCarsModule {

    @Provides
    fun provideUseCaseGetCars(carsRepository: CarsRepository): UseCaseGetCars {
        return UseCaseGetCars(carsRepository)
    }
}