package com.daniel.webmotors_cars_android.di.repository

import com.daniel.webmotors_cars_android.data.datasource.CarsLocalDataSource
import com.daniel.webmotors_cars_android.data.datasource.CarsRemoteDataSource
import com.daniel.webmotors_cars_android.data.repository.CarsRepositoryImplementation
import com.daniel.webmotors_cars_android.domain.repository.CarsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
class CarsRepositoryImplementationModule {

    @Provides
    fun provideCarsRepositoryImplementation(carsRemoteDataSource: CarsRemoteDataSource,
                                carsLocalDataSource: CarsLocalDataSource): CarsRepository {
        return CarsRepositoryImplementation(carsLocalDataSource, carsRemoteDataSource)
    }
}