package com.daniel.webmotors_cars_android.di.api.service

import com.daniel.webmotors_cars_android.data.api.CarsApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit

@Module
@InstallIn(ApplicationComponent::class)
class ServiceModule {

    @Provides
    fun provideServiceCars(retrofit: Retrofit): CarsApiService {
        return retrofit.create(CarsApiService::class.java)
    }
}