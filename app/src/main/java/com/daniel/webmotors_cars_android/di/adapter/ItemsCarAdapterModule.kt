package com.daniel.webmotors_cars_android.di.adapter

import com.daniel.webmotors_cars_android.presentation.adapter.Items_GridRVAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ItemsCarAdapterModule {

    @Provides
    @Singleton
    fun provideAdapterItemsCar(): Items_GridRVAdapter {
        return Items_GridRVAdapter()
    }

}