package com.daniel.webmotors_cars_android.di.viewmodel

import com.daniel.webmotors_cars_android.data.repository.CarsRepositoryImplementation
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetCars
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetListCarsFromLocalDatabase
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetUpdateListCars
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseSaveAllOnLocalDatabase
import com.daniel.webmotors_cars_android.presentation.viewmodel.CarsViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent


@Module
@InstallIn(ApplicationComponent::class)
class CarsViewModelModule {
    @Provides
    fun provideCarsViewModel(getCarsUseCase: UseCaseGetCars,
                             getListCarsFromLocalDatabaseUseCase: UseCaseGetListCarsFromLocalDatabase,
                             getUpdateListCarsUseCase: UseCaseGetUpdateListCars,
                             saveAllOnLocalDatabaseUseCase: UseCaseSaveAllOnLocalDatabase
    ): CarsViewModel {
        return CarsViewModel(getCarsUseCase, getListCarsFromLocalDatabaseUseCase, getUpdateListCarsUseCase, saveAllOnLocalDatabaseUseCase)
    }
}