package com.daniel.webmotors_cars_android.di.usecase

import com.daniel.webmotors_cars_android.domain.repository.CarsRepository
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetUpdateListCars
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
class UseCaseGetUpdateListCarsModule {

    @Provides
    fun provideUseCaseGetUpdateListCars(carsRepository: CarsRepository): UseCaseGetUpdateListCars {
        return UseCaseGetUpdateListCars(carsRepository)
    }
}