package com.daniel.webmotors_cars_android.presentation.utils

object Constant {
    const val VIEW_TYPE_ITEM = 0
    const val VIEW_TYPE_LOADING = 1
}