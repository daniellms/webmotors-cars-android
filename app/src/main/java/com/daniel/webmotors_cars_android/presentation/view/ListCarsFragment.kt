package com.daniel.webmotors_cars_android.presentation.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.os.postDelayed
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daniel.webmotors_cars_android.R
import com.daniel.webmotors_cars_android.data.model.Car
import com.daniel.webmotors_cars_android.data.utils.NetConnection
import com.daniel.webmotors_cars_android.databinding.FragmentListCarsBinding
import com.daniel.webmotors_cars_android.presentation.adapter.Items_GridRVAdapter
import com.daniel.webmotors_cars_android.presentation.utils.Event
import com.daniel.webmotors_cars_android.presentation.viewmodel.CarsViewModel

/**
 * A simple [Fragment] subclass.
 * Use the [ListCarsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListCarsFragment : Fragment() {

    lateinit var listCarsFragmentViewBinding: FragmentListCarsBinding
    private lateinit var carsViewModel: CarsViewModel
    private lateinit var adapterItemsCar: Items_GridRVAdapter
    private lateinit var scrollListener: RecyclerViewLoadMoreScroll
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var netConnection: NetConnection

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        carsViewModel = (activity as CarsActivity).carsViewModel
        adapterItemsCar = (activity as CarsActivity).adapterItemsCar
        netConnection = (activity as CarsActivity).netConnection

        addObservers()
        initRecyclerView()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        listCarsFragmentViewBinding = FragmentListCarsBinding.inflate(
            inflater, container, false)
        return listCarsFragmentViewBinding.root
    }

    private fun addObservers() {
        carsViewModel.mutableLiveDataGetCars.observe(viewLifecycleOwner, this::successGetCars)
        carsViewModel.mutableLiveErrorGetCars.observe(viewLifecycleOwner, this::errorGetCars)
        carsViewModel.mutableLiveDataListUpdateCars.observe(viewLifecycleOwner, this::successGetListUpdateCars)
    }

    private fun successGetCars(carsEvent: Event<ArrayList<Car>>) {
        carsEvent.getContentIfNotHandled()?.let { cars ->
            this.adapterItemsCar.removeLoadingView()
            if (cars.isEmpty()) {
                Toast.makeText(activity, R.string.message_has_not_internet_connection, Toast.LENGTH_LONG).show()
                Handler(Looper.getMainLooper()).postDelayed(Runnable { requireActivity().finish() }, 5000)
            } else {
                adapterItemsCar.addData(cars)
                adapterItemsCar.notifyDataSetChanged()
                listCarsFragmentViewBinding.rvItemsGridCars.adapter = adapterItemsCar
            }
        }
    }

    private fun errorGetCars(messageEvent: Event<String>) {
        messageEvent.getContentIfNotHandled()?.let { message ->
            this.adapterItemsCar.removeLoadingView()
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun successGetListUpdateCars(responseGetCar: Event<ArrayList<Car>>) {
        responseGetCar.getContentIfNotHandled()?.let { listCars ->
            this.adapterItemsCar.removeLoadingView()
            adapterItemsCar.addData(listCars)
            scrollListener.setLoaded()

            listCarsFragmentViewBinding.rvItemsGridCars.post {
                adapterItemsCar.notifyDataSetChanged()
            }
        }
    }

    private fun initRecyclerView() {
        setRvLayoutManager()
        setRvScrollListener()
        adapterItemsCar.onItemClick = { car ->
            val bundle = bundleOf("car" to car)
            findNavController().navigate(R.id.action_listCarsFragment_to_detailCarFragment, bundle)
        }

        if (netConnection.hasConnectionInternet()) {
            if (carsViewModel.isFragmentCallInitRequest()) {
                this.setFragmentCalledFirstTime()
                callGetCars()
            }
        } else {
            if (carsViewModel.isFragmentCallInitRequest()) {
                this.setFragmentCalledFirstTime()
                callGetListCarsFromLocalDatabase()
            }
        }
    }

    private fun setFragmentCalledFirstTime() {
        carsViewModel.setFragmentCallInitRequest()
    }

    private fun setRvScrollListener() {
        scrollListener = RecyclerViewLoadMoreScroll(mLayoutManager as GridLayoutManager, this.carsViewModel.getCurrentPage())
        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (netConnection.hasConnectionInternet()) {
                    callMoreItemNextPageItems()
                } else {
                    scrollListener.setLoaded()
                }
            }
        })

        listCarsFragmentViewBinding.rvItemsGridCars.addOnScrollListener(scrollListener)
    }

    private fun callMoreItemNextPageItems() {
        this.adapterItemsCar.addLoadingView()
        this.carsViewModel.updateNextPage()
        this.scrollListener.updateVisibleThreshold(this.carsViewModel.getCurrentPage())
        this.carsViewModel.getUpdateCars()
    }

    private fun setRvLayoutManager() {
        mLayoutManager = GridLayoutManager(activity, 1)
        listCarsFragmentViewBinding. apply {
            rvItemsGridCars.layoutManager = mLayoutManager
            rvItemsGridCars.setHasFixedSize(true)
            rvItemsGridCars.adapter = adapterItemsCar
        }
    }

    private fun callGetListCarsFromLocalDatabase() {
        this.carsViewModel.getCarsFromLocalDatabase()
    }

    private fun callGetCars() {
        this.adapterItemsCar.addLoadingView()
        this.carsViewModel.getCars()
    }
}