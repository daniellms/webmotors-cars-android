package com.daniel.webmotors_cars_android.presentation.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.daniel.webmotors_cars_android.R
import com.daniel.webmotors_cars_android.data.utils.NetConnection
import com.daniel.webmotors_cars_android.presentation.adapter.Items_GridRVAdapter
import com.daniel.webmotors_cars_android.presentation.viewmodel.CarsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CarsActivity : AppCompatActivity() {

    @Inject lateinit var carsViewModel: CarsViewModel
    @Inject lateinit var adapterItemsCar: Items_GridRVAdapter
    @Inject lateinit var netConnection: NetConnection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cars)
    }
}