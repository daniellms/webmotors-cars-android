package com.daniel.webmotors_cars_android.presentation.view

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

class RecyclerViewLoadMoreScroll : RecyclerView.OnScrollListener {

    private var visibleThreshold = 0
    private val NUMBER_OF_PAGES = 10
    private lateinit var mOnLoadMoreListener: OnLoadMoreListener
    private var isLoading: Boolean = false
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var mLayoutManager: RecyclerView.LayoutManager

    fun setLoaded() {
        isLoading = false
    }

    fun getLoaded(): Boolean {
        return isLoading
    }

    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    fun updateVisibleThreshold(page: Int) {
        visibleThreshold = NUMBER_OF_PAGES * page
    }

    constructor(layoutManager: GridLayoutManager, page: Int) {
        this.mLayoutManager = layoutManager
        visibleThreshold = NUMBER_OF_PAGES * page
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy <= 0) return

        totalItemCount = mLayoutManager.itemCount

        if (mLayoutManager is GridLayoutManager) {
            lastVisibleItem = (mLayoutManager as GridLayoutManager).findLastVisibleItemPosition()
        }

        if (lastVisibleItem == (visibleThreshold-1)) {
            if (!isLoading) {
                isLoading = true
                mOnLoadMoreListener.onLoadMore()
            }
        }
    }
}