package com.daniel.webmotors_cars_android.presentation.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.daniel.webmotors_cars_android.R
import com.daniel.webmotors_cars_android.data.model.Car
import com.daniel.webmotors_cars_android.presentation.utils.Constant
import kotlinx.android.synthetic.main.grid_item_row.view.*
import kotlinx.android.synthetic.main.progress_loading.view.*

class Items_GridRVAdapter() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var mcontext: Context
    private var itemsCells = ArrayList<Car?>()

    var onItemClick: ((Car) -> Unit)? = null

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(itemsCells[adapterPosition]!!)
            }
        }
    }
    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun addData(dataViews: ArrayList<Car>) {
        this.itemsCells.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun getItemAtPosition(position: Int): Car? {
        return itemsCells[position]
    }

    fun addLoadingView() {
        Handler(Looper.getMainLooper()).post {
            itemsCells.add(null)
            notifyItemInserted(itemsCells.size - 1)
        }
    }

    fun removeLoadingView() {
        if (itemsCells.size != 0) {
            itemsCells.removeAt(itemsCells.size - 1)
            notifyItemRemoved(itemsCells.size)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mcontext = parent.context
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.grid_item_row, parent, false)
            ItemViewHolder(view)
        } else {
            val view = LayoutInflater.from(mcontext).inflate(R.layout.progress_loading, parent, false)
            LoadingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return itemsCells.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (itemsCells[position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            val car = itemsCells[position]!!
            setValuesOnItem(holder, car)
        }
    }

    private fun setValuesOnItem(holder: RecyclerView.ViewHolder, car: Car) {
        setImageCarWithGlideProperties(car, holder)
        setCarInformations(car, holder)
    }

    private fun setImageCarWithGlideProperties(car: Car, holder: RecyclerView.ViewHolder) {
        Glide.with(this.mcontext)
            .load(car.image)
            .placeholder(ContextCompat.getDrawable(mcontext, R.drawable.little_car))
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(holder.itemView.ivImageCar)
    }

    @SuppressLint("SetTextI18n")
    private fun setCarInformations(car: Car, holder: RecyclerView.ViewHolder) {
        holder.itemView.tvVersion.text = car.version
        holder.itemView.tvKm.text = car.kM.toString() + " km"
        holder.itemView.tvPrice.text = "R$ " + car.price
    }
}