package com.daniel.webmotors_cars_android.viewmodel

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.daniel.webmotors_cars_android.data.api.CarsApiService
import com.daniel.webmotors_cars_android.data.database.dao.CarsCacheDAO
import com.daniel.webmotors_cars_android.data.datasource.CarsLocalDataSource
import com.daniel.webmotors_cars_android.data.datasource.CarsRemoteDataSource
import com.daniel.webmotors_cars_android.data.repository.CarsRepositoryImplementation
import com.daniel.webmotors_cars_android.domain.repository.CarsRepository
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetCars
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetListCarsFromLocalDatabase
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetUpdateListCars
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseSaveAllOnLocalDatabase
import com.daniel.webmotors_cars_android.presentation.viewmodel.CarsViewModel
import com.google.common.truth.Truth
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito

@RunWith(AndroidJUnit4::class)
class CarsViewModelTest {

    lateinit var carsViewModel: CarsViewModel
    
    val repository = Mockito.mock(CarsRepository::class.java)
    val useCaseGetCars = UseCaseGetCars(repository)
    val useCaseGetListCarsFromLocalDatabase = UseCaseGetListCarsFromLocalDatabase(repository)
    val useCaseGetUpdateListCars = UseCaseGetUpdateListCars(repository)
    val useCaseSaveAllOnLocalDatabase = UseCaseSaveAllOnLocalDatabase(repository)

    @Test
    fun getCurrentPage_currentPageOneReceived_firstCurrentPage() {
        carsViewModel = CarsViewModel(useCaseGetCars,
            useCaseGetListCarsFromLocalDatabase,
            useCaseGetUpdateListCars,
            useCaseSaveAllOnLocalDatabase)
        val resultPage = carsViewModel.getCurrentPage()
        Truth.assertThat(resultPage).isEqualTo(1)
    }

    @Test
    fun updatePage_currentPageTwoReceived_secondCurrentPage() {
        carsViewModel = CarsViewModel(useCaseGetCars,
            useCaseGetListCarsFromLocalDatabase,
            useCaseGetUpdateListCars,
            useCaseSaveAllOnLocalDatabase)
        carsViewModel.updateNextPage()
        val resultPage = carsViewModel.getCurrentPage()
        Truth.assertThat(resultPage).isEqualTo(2)
    }
}